package de.dericon.productsservice.api.dto;

import lombok.Value;

@Value
public class Derived {
  Issuer issuer;
  Underlying underlying;

  @Value
  public static class Issuer {
    String name;
  }

  @Value
  public static class Underlying {
    String name;
  }
}