package de.dericon.productsservice.api.dto;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class ProductResponse {
  String issuerName;
  String underlyingName;
  String id;
  double yield;
}
