package de.dericon.productsservice.api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Value;

@Value
public class ProductRequest {
  @JsonProperty("_id")
  String id;
  Derived derived;
  Ids ids;
  Figures figures;

  public double sideYieldPa() {
    return figures.getSideYieldPa();
  }
}
