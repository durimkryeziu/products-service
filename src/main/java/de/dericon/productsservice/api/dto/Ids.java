package de.dericon.productsservice.api.dto;

import lombok.Value;

@Value
public class Ids {
  String isin;
  String wkn;
  String vwd;
}
