package de.dericon.productsservice.api.dto;

import lombok.Value;

@Value
public class Figures {
  double sideYield;
  double sideYieldPa;
}
