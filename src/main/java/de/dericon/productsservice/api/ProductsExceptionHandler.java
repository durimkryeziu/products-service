package de.dericon.productsservice.api;

import static org.springframework.util.StringUtils.hasText;

import de.dericon.productsservice.application.exception.IncorrectJsonInputException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.multipart.MaxUploadSizeExceededException;

import java.time.Instant;
import lombok.Value;
import lombok.var;

@RestControllerAdvice
public class ProductsExceptionHandler {

  private static final String NO_MESSAGE_AVAILABLE = "No message available";

  @ExceptionHandler({
      HttpMessageNotReadableException.class,
      MissingServletRequestParameterException.class,
      IncorrectJsonInputException.class,
      MaxUploadSizeExceededException.class
  })
  public ResponseEntity<ErrorResponse> handleNotReadableException(Exception e) {
    return create(e, HttpStatus.BAD_REQUEST);
  }

  private ResponseEntity<ErrorResponse> create(Exception e, HttpStatus httpStatus) {
    var errorResponse = new ErrorResponse(
        httpStatus.value(),
        httpStatus.getReasonPhrase(),
        getMessage(e),
        Instant.now()
    );
    return ResponseEntity.status(httpStatus).body(errorResponse);
  }

  private static String getMessage(Exception exception) {
    if (hasText(exception.getLocalizedMessage())) {
      return exception.getLocalizedMessage();
    } else {
      return NO_MESSAGE_AVAILABLE;
    }
  }

  @Value
  public static class ErrorResponse {
    int code;
    String status;
    String message;
    Instant timestamp;
  }
}
