package de.dericon.productsservice.api;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;
import static org.springframework.http.MediaType.MULTIPART_FORM_DATA_VALUE;

import de.dericon.productsservice.api.dto.ProductResponse;
import de.dericon.productsservice.domain.ProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@Api(tags = "Products")
@RequestMapping("/api/products")
public class ProductController {

  private final ProductService productService;

  @Autowired
  public ProductController(ProductService productService) {
    this.productService = productService;
  }

  @ApiOperation("Upload Products' file")
  @PostMapping(consumes = MULTIPART_FORM_DATA_VALUE, produces = APPLICATION_JSON_UTF8_VALUE)
  @ApiResponses(@ApiResponse(code = 400, message = "Bad Request", response = ProductsExceptionHandler.ErrorResponse.class))
  public ResponseEntity<List<ProductResponse>> uploadFile(@ApiParam("Products' JSON file") @RequestParam MultipartFile file) {
    return ResponseEntity.ok(productService.process(file));
  }
}
