package de.dericon.productsservice.domain;

import de.dericon.productsservice.api.dto.ProductResponse;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface ProductService {

  List<ProductResponse> process(MultipartFile file);
}
