package de.dericon.productsservice.application.exception;

public class IncorrectJsonInputException extends RuntimeException {

  public IncorrectJsonInputException(Throwable cause) {
    super("Different file content expected", cause);
  }
}
