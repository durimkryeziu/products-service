package de.dericon.productsservice.application;

import static java.util.Comparator.comparingDouble;
import static java.util.stream.Collectors.toList;

import de.dericon.productsservice.api.dto.ProductRequest;
import de.dericon.productsservice.api.dto.ProductResponse;
import de.dericon.productsservice.application.util.ProductMapper;
import de.dericon.productsservice.domain.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import lombok.var;

@Service
public class ProductServiceImpl implements ProductService {

  private final ProductMapper productMapper;

  @Autowired
  public ProductServiceImpl(ProductMapper productMapper) {
    this.productMapper = productMapper;
  }

  @Override
  public List<ProductResponse> process(MultipartFile file) {
    var products = productMapper.mapFrom(file);
    sortBySideYieldPaInDescendingOrder(products);
    return products.stream().map(productMapper::map).collect(toList());
  }

  private void sortBySideYieldPaInDescendingOrder(List<ProductRequest> products) {
    var comparator = comparingDouble(ProductRequest::sideYieldPa);
    products.sort(comparator.reversed());
  }
}
