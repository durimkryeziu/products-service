package de.dericon.productsservice.application.util;

import static java.util.Arrays.asList;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.dericon.productsservice.api.dto.ProductRequest;
import de.dericon.productsservice.api.dto.ProductResponse;
import de.dericon.productsservice.application.exception.IncorrectJsonInputException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import lombok.var;

@Slf4j
@Component
public class ProductMapper {

  private final ObjectMapper objectMapper;

  @Autowired
  public ProductMapper(ObjectMapper objectMapper) {
    this.objectMapper = objectMapper;
  }

  @SneakyThrows(IOException.class)
  public List<ProductRequest> mapFrom(MultipartFile file) {
    try {
      return asList(objectMapper.readValue(file.getInputStream(), ProductRequest[].class));
    } catch (JsonParseException | JsonMappingException e) {
      log.error("JSON Parse failed: ", e);
      throw new IncorrectJsonInputException(e);
    }
  }

  public ProductResponse map(ProductRequest productRequest) {
    var derived = productRequest.getDerived();
    return ProductResponse.builder()
        .issuerName(derived.getIssuer().getName())
        .underlyingName(derived.getUnderlying().getName())
        .id(productRequest.getIds().getIsin())
        .yield(productRequest.sideYieldPa())
        .build();
  }
}
