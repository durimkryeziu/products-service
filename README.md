# Products Service

## Build
- Clone this repository
- `cd products-service`
- `docker build -t products-service .`

## Run
- `docker run -d --name products-service -p 8080:8080 products-service`