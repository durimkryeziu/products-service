FROM azul/zulu-openjdk-alpine:8u192 as build
ENV APP_WORKSPACE=/workspace/app/
WORKDIR $APP_WORKSPACE
COPY gradlew .
COPY gradle gradle
COPY build.gradle settings.gradle $APP_WORKSPACE
COPY lombok.config lombok.config
COPY src src
ENV GRADLE_OPTS -Dorg.gradle.daemon=false -Dorg.gradle.internal.launcher.welcomeMessageEnabled=false
RUN ./gradlew build && mkdir -p build/dependency && (cd build/dependency; jar -xf ../libs/*.jar)

FROM azul/zulu-openjdk-alpine:8u192
VOLUME /tmp
ENV DEPENDENCY=/workspace/app/build/dependency
COPY --from=build $DEPENDENCY/BOOT-INF/lib /app/lib
COPY --from=build $DEPENDENCY/META-INF /app/META-INF
COPY --from=build $DEPENDENCY/BOOT-INF/classes /app
EXPOSE 8080
ENTRYPOINT ["java", "-cp", "app:app/lib/*", \
    "-XX:+UnlockExperimentalVMOptions", "-XX:+UseCGroupMemoryLimitForHeap", \
    "de.dericon.productsservice.ProductsServiceApplication"]